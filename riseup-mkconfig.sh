#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# This script creates an OpenVPN configuration file that allows you to connect
# to RiseupVPN without using the graphical Bitmask client.
#
# The generated configuration will cause OpenVPN to attempt to connect to each
# gateway in order of their speed (as reported by Riseup) until a successful
# connection is made. UDP port 1194 is used by default; if you are having
# issues with censorship, try changing "1194 udp" below to "443 tcp".
#
# Before using this script, you will have to get RiseupVPN's CA certificate from
# https://0xacab.org/leap/bitmask-vpn/-/raw/main/providers/riseup/riseup-ca.crt,
# and save it as "riseup-ca.crt" in the directory you plan to invoke this script
# from. (You can use curl or wget for this.)
#
# More information about RiseupVPN is available at https://riseup.net/en/vpn.
# An alternative script that does (almost) the same thing as this one is
# available at https://github.com/BarbossHack/RiseupVPN-OpenVPN.
#
# Example usage:
#
# wget https://0xacab.org/leap/bitmask-vpn/-/raw/main/providers/riseup/riseup-ca.crt
# ./riseup-mkconfig.sh
# sudo openvpn --config ./riseup.conf

if [[ $# -ne 0 ]]
then
	echo "usage: $0"
	exit 2
fi

CA=./riseup-ca.crt
CONFIG=./riseup.conf

mapfile -t gw < <(curl --silent --cacert "$CA" --capath ':' -- 'https://api.black.riseup.net/3/config/eip-service.json' | jq --raw-output '.gateways[].ip_address')

cert=$(curl --silent --cacert "$CA" --capath ':' -- 'https://api.black.riseup.net/3/cert')
key=$(openssl rsa -in <(echo "$cert"))
cert=$(openssl x509 -in <(echo "$cert"))

umask 0077
( IFS=$'\n'; cat << EOF > "$CONFIG" )
client
nobind
dev tun

${gw[*]/*/remote & 1194 udp}

remote-cert-tls server
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-128-CBC-SHA
auth SHA1

pull-filter ignore "ifconfig-ipv6"
pull-filter ignore "route-ipv6"

<ca>
$(< "$CA")
</ca>

<cert>
$cert
</cert>

<key>
$key
</key>
EOF
