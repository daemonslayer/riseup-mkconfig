riseup-mkconfig.sh
==================

This script creates an OpenVPN configuration file that allows you to connect to
RiseupVPN without using the graphical Bitmask client.

See the long comment in [riseup-mkconfig.sh](./riseup-mkconfig.sh) for example
usage and more info.
